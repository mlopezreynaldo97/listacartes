package com.miguel.llistacartes;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by 23895346w on 01/12/17.
 */

@Database(entities = {Cartes.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context){

        if (INSTANCE == null){

            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "dbCartes").build();

        }

        return INSTANCE;
    }

    public abstract CartesDAO getCartesDAO();


}
