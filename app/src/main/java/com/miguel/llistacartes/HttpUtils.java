package com.miguel.llistacartes;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by 23895346w on 20/10/17.
 */

public class HttpUtils {


    public static String get(String dataUrl) throws IOException {
        URL url = new URL(dataUrl);
        String response;

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        try {

            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());
            response = readStream(in);

        } finally {

            httpURLConnection.disconnect();
        }
            return response;
        }

    private static String readStream(InputStream in) throws IOException {

        InputStreamReader is = new InputStreamReader(in);
        BufferedReader rd = new BufferedReader(is);

        String line;
        StringBuilder response = new StringBuilder();

        while ((line = rd.readLine()) != null) {

            response.append(line);
            response.append('\r');

        }

        rd.close();

        return response.toString();
    }
}
