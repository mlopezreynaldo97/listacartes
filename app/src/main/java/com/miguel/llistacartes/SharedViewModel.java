package com.miguel.llistacartes;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SharedViewModel extends AndroidViewModel{

    private final Application app;
    private final AppDatabase appDatabase;
    private final CartesDAO cartesDAO;
    private LiveData<List<Cartes>> cartes;


    public SharedViewModel(Application application){

        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.cartesDAO = appDatabase.getCartesDAO();

    }

    public LiveData<List<Cartes>> getCartes(){

        if(cartes == null){
//            cartes = new MutableLiveData<>();
//            reload();
            return cartesDAO.getCartes();
        }

        return cartes;

    }

    public void reload() {

        RefreshData task = new RefreshData();
        task.execute();

    }

    private class RefreshData extends AsyncTask<Void, Void, ArrayList<Cartes>>{

        @Override
        protected ArrayList<Cartes> doInBackground(Void... voids){

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());

            //Preferencia funcionamiento S1 = Clave, S2 = Valor
            String rarity = preferences.getString("tipos_consulta","sin_filtro");

            MagicAPI api = new MagicAPI();
            ArrayList<Cartes> result = null;

            if (rarity.equals("sin_filtro")){
                result = api.getMagicCartes();
            } else {
                result = api.getMagicRarity(rarity);
            }

            Log.d("DEBUG", result != null ? result.toString() : null);

            cartesDAO.deleteCartes();
            cartesDAO.addCartes(result);

            return result;

        }


//        @Override
//        protected void onPostExecute(ArrayList<Cartes> data) {
//            super.onPostExecute(data);
//        }
    }

}
