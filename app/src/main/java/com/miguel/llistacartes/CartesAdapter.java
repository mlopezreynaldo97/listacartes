package com.miguel.llistacartes;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miguel.llistacartes.databinding.CartasNewRowBinding;

import java.util.List;


public class CartesAdapter extends ArrayAdapter<Cartes>{

    private CartasNewRowBinding binding;

    public CartesAdapter(Context context, int resource, List<Cartes> objects){
        super(context,resource,objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Cartes cartes = getItem(position);
        Log.w("XXXX", cartes.toString());

        binding = null;

        if(convertView == null){

            LayoutInflater inflater = LayoutInflater.from(getContext());
            binding = DataBindingUtil.inflate(inflater, R.layout.cartas_new_row,parent, false);

        } else {

            binding = DataBindingUtil.getBinding(convertView);

        }

        binding.tituloCartes.setText(cartes.getName());
        binding.txtView.setText(cartes.getText());
        binding.tvRarity.setText(cartes.getRarity());

        Glide.with(getContext()).load(cartes.getImageUrl()).into(binding.imgCarta);

        return binding.getRoot();
    }
}
