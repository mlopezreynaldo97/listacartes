package com.miguel.llistacartes;

import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Entity;

import java.io.Serializable;


/**
 * Created by 23895346w on 20/10/17.
 */
@Entity
public class Cartes implements Serializable{

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private String text;
    private String rarity;
    private String color;
    private String imageUrl;

    public Cartes() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }



    @Override
    public String toString() {
        return "Cartes{" +
                "name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", rarity='" + rarity + '\'' +
                ", color=" + color +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
