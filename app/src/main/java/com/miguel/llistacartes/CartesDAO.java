package com.miguel.llistacartes;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CartesDAO {

    @Query("select * from Cartes")
    LiveData<List<Cartes>> getCartes();

    @Insert
    void addCarta(Cartes cartes);

    @Insert
    void addCartes(List<Cartes> cartes);

    @Delete
    void deleteCarta(Cartes cartes);

    @Query("DELETE FROM Cartes")
    void  deleteCartes();

}
