package com.miguel.llistacartes;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


public class MagicAPI {

    private final String BASE_URL = "https://api.magicthegathering.io/v1/cards";

    public ArrayList<Cartes> getMagicCartes(){

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .build();
        String url = builtUri.toString();

        return doCall(url);

    }


    public ArrayList<Cartes> getMagicRarity(String rarity){

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("rarity", rarity)
                .build();
        String url = builtUri.toString();

        return doCall(url);

    }

    public ArrayList<Cartes> getMagicColors(String color){

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("color", color)
                .build();
        String url = builtUri.toString();

        return doCall(url);

    }



    private ArrayList<Cartes> doCall(String url){

        try{

            String respuestaJson = HttpUtils.get(url);
            return processJSON(respuestaJson);

        } catch (IOException e){

            e.printStackTrace();

        }

        return null;

    }

    private ArrayList<Cartes> processJSON(String responseJson) {

        ArrayList<Cartes> cartesList = new ArrayList<>();

        try {

            JSONObject data = new JSONObject(responseJson);
            JSONArray jsonCartes = data.getJSONArray("cards");

            for (int i = 0; i < jsonCartes.length(); i++) {
                JSONObject jsonCarta = jsonCartes.getJSONObject(i);

                Cartes carta = new Cartes();

                carta.setName(jsonCarta.getString("name"));
                carta.setRarity(jsonCarta.getString("rarity"));

                if(jsonCarta.has("text")){

                    carta.setText(jsonCarta.getString("text"));

                } else {

                    carta.setText("No Description Avaliable");

                }

                if(jsonCarta.has("imageUrl")){

                    carta.setImageUrl(jsonCarta.getString("imageUrl"));

                } else {

                    carta.setImageUrl("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ac/No_image_available.svg/2000px-No_image_available.svg.png");
                }

                cartesList.add(carta);


            }

        } catch (JSONException e) {

            e.printStackTrace();
        }

        return cartesList;


    }

}
