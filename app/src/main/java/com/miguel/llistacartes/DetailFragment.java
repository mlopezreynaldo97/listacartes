package com.miguel.llistacartes;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.miguel.llistacartes.databinding.FragmentDetailBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailFragment extends Fragment {

    private FragmentDetailBinding binding;
    private View view;

    public DetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if( i!=null ){

            Cartes card = (Cartes) i.getSerializableExtra("cards");

            if( card != null ){

                showCard(card);

            }
        }

        return view;
    }

    private void showCard(Cartes card) {
        binding.txtNameCard.setText(card.getName());
        binding.txtTextCard.setText(card.getText());
        Glide.with(getContext()).load(card.getImageUrl()).into(binding.imgDetailCard);

    }
}
