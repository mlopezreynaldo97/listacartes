package com.miguel.llistacartes;

import android.arch.lifecycle.LifecycleFragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.miguel.llistacartes.databinding.CartasNewRowBinding;
import com.miguel.llistacartes.databinding.FragmentMainBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivityFragment extends LifecycleFragment{

    /*Fase 1 Terminada en el dia: 21/10/2017*/

    private ArrayList<Cartes> items;
    private CartesAdapter adaptador;
//    private CartasNewRowBinding binding;
    private FragmentMainBinding binding;
    private SharedPreferences preferences;
    private SharedViewModel model;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        refresh();
    }

    boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentMainBinding.inflate(inflater);
        View view = binding.getRoot();

        items = new ArrayList<>();

        adaptador = new CartesAdapter(getContext(), R.layout.cartas_new_row, items);

        binding.lvwCartes.setAdapter(adaptador);

        binding.lvwCartes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Cartes itemAtPosition = (Cartes) adapterView.getItemAtPosition(i);

//                if(!esTablet()) {
                    Intent in = new Intent(getContext(), Detail.class);
                    in.putExtra("cards", itemAtPosition);
                    startActivity(in);
//                }

            }
        });


        model = ViewModelProviders.of(this).get(SharedViewModel.class);
        model.getCartes().observe(this, new Observer<List<Cartes>>() {
            @Override
            public void onChanged(@Nullable List<Cartes> cartes) {
                adaptador.clear();
                adaptador.addAll(cartes);
            }
        });

        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_lv_cartes, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {

        model.reload();

    }

}